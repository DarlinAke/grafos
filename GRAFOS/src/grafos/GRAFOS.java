/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

/**
 *
 * @author adriana
 */
public class GRAFOS {

   public static Graph getCities() {
        Node camp = new Node("Campeche");
        Node CHAM = new Node("champoton");
        Node FCP = new Node("F. Carrllo P");
        
        camp.addEdge(new Edge(camp, CHAM,57));
        camp.addEdge(new Edge(camp, FCP, 174));
        camp.addEdge(new Edge(CHAM, FCP, 93));
        
        Graph graph = new Graph();
        graph.addNode(camp);
        graph.addNode(CHAM);
        graph.addNode(FCP);
        return graph;
    }
    public static void main(String[] args) {
        Graph graph = getCities();
        System.out.println(graph);
        
    }
}